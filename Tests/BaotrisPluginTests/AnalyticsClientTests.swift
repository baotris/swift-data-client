import Foundation
import XCTest
@testable import BaotrisPlugin

class AnalyticsClientTests: XCTestCase {
    func testQuicklyDisabledPlugin() throws {
        let json = """
            {
                "pluginEnabled": false,
                "dataEndpoint": "https://localhost/"
            }
        """
        let config = try! RemoteConfig(json)
        var callback: ((RemoteConfig) -> Void)?

        let client = AnalyticsClient(
            configCallback:  { c in callback = c },
            configComplete:  { (client, enabled) in XCTAssertFalse(enabled) },
            randomGenerator: { 0.0 }
        )

        XCTAssertNotNil(client.dataClient)
        callback!(config)
        XCTAssertNil(client.dataClient)
        try client.set("a1", 1)
        try client.event("test1", "k1", "v1")
        try client.set("a2", 2)
        try client.event("test2", "k1", "v1")
    }

    func testSlowlyDisabledPlugin() throws {
        let json = """
            {
                "pluginEnabled": false,
                "dataEndpoint": "https://localhost/"
            }
        """
        let config = try! RemoteConfig(json)
        var callback: ((RemoteConfig) -> Void)?

        let client = AnalyticsClient(
            configCallback:  { c in callback = c },
            configComplete:  { (client, enabled) in XCTAssertFalse(enabled) },
            randomGenerator: { 0.0 }
        )

        XCTAssertNotNil(client.dataClient)
        try client.set("a1", 1)
        try client.event("test1", "k1", "v1")
        callback!(config)
        XCTAssertNil(client.dataClient)
        try client.set("a2", 2)
        try client.event("test2", "k1", "v1")
    }

    func testSampleDisabledPlugin() throws {
        let json = """
            {
                "pluginEnabled": true,
                "sampleRate": 0.1,
                "dataEndpoint": "https://localhost/"
            }
        """
        let config = try! RemoteConfig(json)
        var callback: ((RemoteConfig) -> Void)?

        let client = AnalyticsClient(
            configCallback:  { c in callback = c },
            configComplete:  { (client, enabled) in XCTAssertFalse(enabled) },
            randomGenerator: { 0.2 }
        )

        XCTAssertNotNil(client.dataClient)
        try client.set("a1", 1)
        try client.event("test1", "k1", "v1")
        callback!(config)
        XCTAssertNil(client.dataClient)
        try client.set("a2", 2)
        try client.event("test2", "k1", "v1")
    }

    func testSampleEnabledPlugin() throws {
        let json = """
            {
                "pluginEnabled": true,
                "sampleRate": 0.1,
                "dataEndpoint": "https://localhost/"
            }
        """
        let config = try! RemoteConfig(json)
        var callback: ((RemoteConfig) -> Void)?

        let client = AnalyticsClient(
            configCallback:  { c in callback = c },
            configComplete:  { (client, enabled) in XCTAssertTrue(enabled) },
            randomGenerator: { 0.05 }
        )

        XCTAssertNotNil(client.dataClient)
        try client.set("a1", 1)
        try client.event("test1", "k1", "v1")
        callback!(config)
        XCTAssertNotNil(client.dataClient)
        try client.set("a2", 2)
        try client.event("test2", "k1", "v1")
        // TODO: client.stop() // stop the uploader
    }
}
