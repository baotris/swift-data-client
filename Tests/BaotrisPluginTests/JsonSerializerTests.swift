import Foundation
import XCTest
@testable import BaotrisPlugin

class JsonSerializerTests: XCTestCase {
    var output: Buffer?
    var json: JsonSerializer?

    override func setUp() {
        output = Buffer()
        json   = JsonSerializer(output: output!)
    }

    func assertBuffer(_ expectedValue: String) {
        XCTAssertEqual(output!.buffer, expectedValue)
    }

    func testSimpleString() throws {
        try json!.stringValue("test")
        assertBuffer("\"test\"\n")
    }

    func testUnicodeString() throws {
        try json!.stringValue("Flowers 💐")
        assertBuffer("\"Flowers \\uD83D\\uDC90\"\n")
    }

    func testZeroInt() throws {
        try json!.intValue(0)
        assertBuffer("0\n")
    }

    func testMaxInt() throws {
        try json!.intValue(Int32.max)
        assertBuffer("2147483647\n")
    }

    func testMinInt() throws {
        try json!.intValue(Int32.min)
        assertBuffer("-2147483648\n")
    }

    func testZeroLong() throws {
        try json!.longValue(0)
        assertBuffer("0\n") // Note: Same as intValue(0)
    }

    func testMaxLong() throws {
        try json!.longValue(Int64.max)
        // Note: Javascript uses floating-point, so cannot exactly represent more than 2^53
        assertBuffer("9223372036854775807\n")
    }

    func testMinLong() throws {
        try json!.longValue(Int64.min)
        // Note: Javascript uses floating-point, so cannot exactly represent more than 2^53
        assertBuffer("-9223372036854775808\n")
    }

    func testZeroFloat() throws {
        try json!.floatValue(0.0)
        assertBuffer("0.0\n")
    }

    func testSimpleFloat() throws {
        try json!.floatValue(-1.000025)
        assertBuffer("-1.000025\n")
    }

    func testZeroDouble() throws {
        try json!.doubleValue(0.0)
        assertBuffer("0.0\n")
    }

    func testSimpleDouble() throws {
        try json!.doubleValue(-1.000000000000025)
        assertBuffer("-1.000000000000025\n")
    }

    func testAny() throws {
        try json!.value("test")
        try json!.value(Int16.max)
        try json!.value(Int32.max)
        try json!.value(Int64.max)
        try json!.value(Int.max)
        try json!.value(Float32(-1.000025))
        try json!.value(Float64(-1.000000000000025))
        try json!.value(true)
        try json!.value(false)
        try json!.value(nil)
        assertBuffer("""
\"test\"\n\
32767\n\
2147483647\n\
9223372036854775807\n\
9223372036854775807\n\
-1.000025\n\
-1.000000000000025\n\
true\n\
false\n\
null\n
""")
    }

    func testNullValue() throws {
        try json!.nullValue()
        assertBuffer("null\n")
    }

    func testEmptyArray() throws {
        try json!.startArray()
        try json!.endArray()
        assertBuffer("[]\n")
    }

    func testEmptyObject() throws {
        try json!.startObject()
        try json!.endObject()
        assertBuffer("{}\n")
    }

    func testNestedArray() throws {
        try json!.startArray()
        try json!.startArray()
        try json!.endArray()
        try json!.endArray()
        assertBuffer("[[]]\n")
    }

    func testNestedObject() throws {
        try json!.startObject()
        try json!.objectKey("test")
        try json!.startObject()
        try json!.endObject()
        try json!.endObject()
        assertBuffer("{\"test\":{}}\n")
    }

    func testStream() throws {
        try json!.stringValue("test")
        try json!.stringValue("Flowers 💐")
        try json!.intValue(0)
        try json!.startArray()
        try json!.endArray()
        try json!.startObject()
        try json!.endObject()
        try json!.startArray()
        try json!.startArray()
        try json!.endArray()
        try json!.endArray()
        try json!.startObject()
        try json!.objectKey("test")
        try json!.startArray()
        try json!.endArray()
        try json!.endObject()
        assertBuffer("\"test\"\n\"Flowers \\uD83D\\uDC90\"\n0\n[]\n{}\n[[]]\n{\"test\":[]}\n")
    }
}
