import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(AnalyticsClientTests.allTests),
        testCase(JsonSerializerTests.allTests),
        testCase(RemoteConfigTests.allTests),
        testCase(SimpleDataClientTests.allTests),
    ]
}
#endif
