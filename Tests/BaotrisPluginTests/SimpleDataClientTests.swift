import Foundation
import XCTest
@testable import BaotrisPlugin

class SimpleDataClientTests: XCTestCase {
    var dc: SimpleDataClient?

    override func setUp() {
        dc = SimpleDataClient(config: DefaultDataClientConfig())
    }

    func assertFlush(_ expected: String) throws {
        let records = try dc!._flush()
        let output  = Buffer()
        let json    = JsonSerializer(output: output)
        for r in records {
            r.print(output: json)
        }
        XCTAssertEqual(output.buffer, expected)
    }

    func testSimpleEvent() throws {
        try dc!.event("test")
        try assertFlush("[\"test\",{}]\n")
        try assertFlush("")
    }

    func testSimpleEventWithParameter() throws {
        try dc!.event("test", "k1", "v1")
        try assertFlush("[\"test\",{\"k1\":\"v1\"}]\n")
        try assertFlush("")
    }

    func testSimpleEventWithParameters() throws {
        try dc!.event(
            "test",
            "k1", "v1",
            "k2", Int16.max,
            "k3", Int32.max,
            "k4", Int64.max,
            "k5", Int.max,
            "k6", Float32(-1.000025),
            "k7", -1.000000000000025,
            "k8", true,
            "k9", false
        )
        try assertFlush("""
[\"test\",{\
\"k1\":\"v1\",\
\"k2\":32767,\
\"k3\":2147483647,\
\"k4\":9223372036854775807,\
\"k5\":9223372036854775807,\
\"k6\":-1.000025,\
\"k7\":-1.000000000000025,\
\"k8\":true,\
\"k9\":false\
}]\n
""")
        try assertFlush("")
    }

    func testAttributes() throws {
        try dc!.set("a1", "test1")
        try dc!.event("test")
        try assertFlush("{\"a1\":\"test1\"}\n[\"test\",{}]\n")
        try assertFlush("")
        try dc!.event("test2")
        try assertFlush("{\"a1\":\"test1\"}\n[\"test2\",{}]\n")
    }

    func testAttributeChange() throws {
        try dc!.set("a1", "test1")
        try dc!.set("a1", "test2")
        try dc!.event("test")
        try assertFlush("{\"a1\":\"test2\"}\n[\"test\",{}]\n")
        try dc!.event("test")
        try dc!.set("a1", 123)
        try dc!.event("test2")
        try dc!.set("a1", 123)
        try dc!.event("test3")
        try assertFlush("{\"a1\":\"test2\"}\n[\"test\",{}]\n{\"a1\":123}\n[\"test2\",{}]\n[\"test3\",{}]\n")
    }

    func testDisabledEvent() throws {
        // TODO: use DataClientConfig -- see Android
        let json = """
            {
                "dataEndpoint": "https://localhost/",
                "attributes": [
                    {
                        "attribute": "a1",
                        "enabled": false
                    },
                    {
                        "attribute": "a2",
                        "enabled": true
                    }
                ],
                "events": [
                    {
                        "event": "test1",
                        "enabled": false
                    },
                    {
                        "event": "test2",
                        "enabled": true
                    }
                ]
            }
        """
        let rc = try RemoteConfig(json)
        self.dc = SimpleDataClient(config: rc)
        try dc!.set("a1", 1)
        try dc!.set("a2", 2)
        try dc!.event("test1", "k1", "v1")
        try dc!.event("test2", "k1", "v1")
        try assertFlush("{\"a2\":2}\n[\"test2\",{\"k1\":\"v1\"}]\n")
    }
}
