import Foundation
import XCTest
@testable import BaotrisPlugin

class RemoteConfigTests: XCTestCase {
    func testMinimalConfig() throws {
        let json = """
        {
            "dataEndpoint": "https://localhost/"
        }
        """
        let rc = try RemoteConfig(json)
        XCTAssertEqual(rc.json.dataEndpoint, "https://localhost/")
        XCTAssertNil(rc.json.pluginEnabled)
        XCTAssertNil(rc.json.sessionTimeout)
        XCTAssertNil(rc.json.sampleRate)
        XCTAssertNil(rc.json.attributes)
        XCTAssertNil(rc.json.events)
        XCTAssertEqual(rc.pluginEnabled,  RemoteConfig.pluginEnabledDefault)
        XCTAssertEqual(rc.sessionTimeout, RemoteConfig.sessionTimeoutDefault)
        XCTAssertEqual(rc.sampleRate,     RemoteConfig.sampleRateDefault)
        XCTAssertEqual(rc.flushFrequency, RemoteConfig.flushFrequencyDefault)
        XCTAssertEqual(rc.flushBatchSize, RemoteConfig.flushBatchSizeDefault)
        XCTAssertEqual(rc.attributeEnabled("test"), true)
        XCTAssertEqual(rc.eventEnabled("test"), true)
    }

    func testPartialFlushCondition() throws {
        let json = """
            {
                "dataEndpoint": "https://localhost/",
                "flushCondition": {
                    "frequency": 60
                }
            }
        """
        let rc = try RemoteConfig(json)
        XCTAssertNil(rc.json.pluginEnabled)
        XCTAssertNil(rc.json.sessionTimeout)
        XCTAssertNil(rc.json.sampleRate)
        XCTAssertNil(rc.json.attributes)
        XCTAssertNil(rc.json.events)
        XCTAssertEqual(rc.pluginEnabled,  RemoteConfig.pluginEnabledDefault)
        XCTAssertEqual(rc.sessionTimeout, RemoteConfig.sessionTimeoutDefault)
        XCTAssertEqual(rc.sampleRate,     RemoteConfig.sampleRateDefault)
        XCTAssertEqual(rc.flushFrequency, 60)
        XCTAssertEqual(rc.flushBatchSize, RemoteConfig.flushBatchSizeDefault)
        XCTAssertEqual(rc.attributeEnabled("test"), true)
        XCTAssertEqual(rc.eventEnabled("test"), true)
    }

    func testDeserialization() throws {
        let json = """
            {
                "dataEndpoint": "https://localhost/",
                "pluginEnabled": true,
                "sessionTimeout": 600,
                "sampleRate": 0.125,
                "flushCondition": {
                    "frequency": 120,
                    "numOfRecords": 100
                },
                "attributes": [
                    {
                        "attribute": "appInfo.appId",
                        "enabled": true
                    },
                    {
                        "attribute": "appInfo.appVersion",
                        "enabled": false
                    },
                    {
                        "attribute": "deviceInfo.deviceModel",
                        "enabled": true
                    }
                ],
                "events": [
                    {
                        "event": "pageLoaded",
                        "enabled": true
                    },
                    {
                        "event": "buttonClicked",
                        "enabled": false
                    }
                ]
            }
        """
        let rc = try RemoteConfig(json)
        XCTAssertEqual(rc.json.pluginEnabled, true)
        XCTAssertEqual(rc.json.sessionTimeout, 600)
        XCTAssertEqual(rc.json.sampleRate, 0.125)
        XCTAssertEqual(rc.json.flushCondition?.frequency, 120)
        XCTAssertEqual(rc.json.flushCondition?.numOfRecords, 100)
        XCTAssertEqual(rc.json.attributes?[0].attribute, "appInfo.appId")
        XCTAssertEqual(rc.json.attributes?[0].enabled, true)
        XCTAssertEqual(rc.json.attributes?[1].attribute, "appInfo.appVersion")
        XCTAssertEqual(rc.json.attributes?[1].enabled, false)
        XCTAssertEqual(rc.json.attributes?[2].attribute, "deviceInfo.deviceModel")
        XCTAssertEqual(rc.json.attributes?[2].enabled, true)
        XCTAssertEqual(rc.json.events?[0].event, "pageLoaded")
        XCTAssertEqual(rc.json.events?[0].enabled, true)
        XCTAssertEqual(rc.json.events?[1].event, "buttonClicked")
        XCTAssertEqual(rc.json.events?[1].enabled, false)

        XCTAssertEqual(rc.pluginEnabled, true)
        XCTAssertEqual(rc.sessionTimeout, 600)
        XCTAssertEqual(rc.sampleRate, 0.125)
        XCTAssertEqual(rc.flushFrequency, 120)
        XCTAssertEqual(rc.flushBatchSize, 100)
        XCTAssertEqual(rc.attributeEnabled("test"), true)
        XCTAssertEqual(rc.attributeEnabled("appInfo.appId"), true)
        XCTAssertEqual(rc.attributeEnabled("appInfo.appVersion"), false)
        XCTAssertEqual(rc.attributeEnabled("deviceInfo.deviceModel"), true)
        XCTAssertEqual(rc.eventEnabled("test"), true)
        XCTAssertEqual(rc.eventEnabled("pageLoaded"), true)
        XCTAssertEqual(rc.eventEnabled("buttonClicked"), false)
    }
}
