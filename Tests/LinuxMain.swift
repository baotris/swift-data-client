import XCTest

import BaotrisPluginTests

var tests = [XCTestCaseEntry]()
tests += BaotrisPluginTests.allTests()
XCTMain(tests)
