import Foundation

public class SimpleDataClient: DataClient {
    init(config: DataClientConfig) {
        self.config = config
    }

    public func set(_ attribute: String, _ value: Bool) throws {
        try setAttribute(attribute, value)
    }

    public func set(_ attribute: String, _ value: Float64) throws {
        try setAttribute(attribute, value)
    }

    public func set(_ attribute: String, _ value: Float32) throws {
        try setAttribute(attribute, value)
    }

    public func set(_ attribute: String, _ value: Int16) throws {
        try setAttribute(attribute, value)
    }

    public func set(_ attribute: String, _ value: Int32) throws {
        try setAttribute(attribute, value)
    }

    public func set(_ attribute: String, _ value: Int64) throws {
        try setAttribute(attribute, value)
    }

    public func set(_ attribute: String, _ value: Int) throws {
        try setAttribute(attribute, value)
    }

    public func set(_ attribute: String, _ value: String) throws {
        try setAttribute(attribute, value)
    }

    public func unset(_ attribute: String) throws {
        lock.lock()
        defer { lock.unlock() }
        guard currentEvent == nil else {
            throw DataClientError.InvalidState
        }
        allAttributes[attribute] = nil
        attributeChanges[attribute] = UnsetSentinel()
    }

    public func startEvent(_ event: String) throws {
        lock.lock() // Note: lock.unlock() is in endEvent()
        guard currentEvent == nil else {
            lock.unlock()
            throw DataClientError.InvalidState
        }

        currentEvent        = event
        currentEventEnabled = config.eventEnabled(event)
        currentParameters.removeAll() // TODO: remove? not really necessary
    }

    public func endEvent() throws {
        lock.lock()
        defer { lock.unlock() }

        newEvent.lock()
        defer { newEvent.unlock() }

        guard let event = currentEvent else {
            throw DataClientError.InvalidState
        }

        if currentEventEnabled {
            if let hook = eventHook {
                hook(self)
            }

            if records.isEmpty {
                logSetAttributes(allAttributes)
            } else {
                logSetAttributes(attributeChanges)
            }
            attributeChanges.removeAll()

            let record = Event(event: event, parameters: currentParameters)
            if config.schemaMatch(record) {
                records.append(record)
            } else {
//                throw error
            }
        }

        self.currentEvent = nil
        self.currentParameters.removeAll()

        if currentEventEnabled {
            newEvent.signal()
            self.currentEventEnabled = false // TODO: not really necessary
        }

        lock.unlock() // Note: lock.lock() is in startEvent()
    }

    public func setEventHook(_ eventHook: @escaping (DataClient) -> Void) {
        self.eventHook = eventHook
    }

    func flush() {
        lock.lock()
        defer { lock.unlock() }

        newEvent.lock()
        defer { newEvent.unlock() }

        self.shouldFlush = true
        self.newEvent.signal()
    }
    
    func _flush() throws -> [Record] {
        guard currentEvent == nil else {
            throw DataClientError.InvalidState
        }
        let records = self.records
        self.records = [Record]()
        self.attributeChanges.removeAll()
        return records
    }

    public func eventParameter(_ parameter: String, _ value: Bool) throws {
        try setEventParameter(parameter, value)
    }

    public func eventParameter(_ parameter: String, _ value: Float64) throws {
        try setEventParameter(parameter, value)
    }

    public func eventParameter(_ parameter: String, _ value: Float32) throws {
        try setEventParameter(parameter, value)
    }

    public func eventParameter(_ parameter: String, _ value: Int16) throws {
        try setEventParameter(parameter, value)
    }
    
    public func eventParameter(_ parameter: String, _ value: Int32) throws {
        try setEventParameter(parameter, value)
    }

    public func eventParameter(_ parameter: String, _ value: Int64) throws {
        try setEventParameter(parameter, value)
    }

    public func eventParameter(_ parameter: String, _ value: Int) throws {
        try setEventParameter(parameter, value)
    }

    public func eventParameter(_ parameter: String, _ value: String) throws {
        try setEventParameter(parameter, value)
    }

    internal var config              : DataClientConfig
    private  var lock                = NSRecursiveLock()
    private  var newEvent            = NSCondition()
    private  var eventHook           : ((DataClient) -> Void)?
    private  var currentEvent        : String?
    private  var currentEventEnabled = false
    private  var currentParameters   = [(String, Any)]()
    private  var allAttributes       = [String: Any]()
    private  var attributeChanges    = [String: Any]()
    private  var records             = [Record]()
    private  var shouldFlush         = false

    private func setAttribute(_ attribute: String, _ value: Any) throws {
        lock.lock()
        defer { lock.unlock() }

        guard currentEvent == nil else {
            throw DataClientError.InvalidState
        }

        if config.attributeEnabled(attribute) {
            if let oldValue = allAttributes.updateValue(value, forKey: attribute) {
                if try !compare(value, oldValue) {
                    attributeChanges[attribute] = value
                }
            }
        }
    }

    private func compare(_ value1: Any, _ value2: Any) throws -> Bool {
        switch value1 {
        case let string1 as String:
            if let string2 = value2 as? String {
                return string1 == string2
            } else {
                return false
            }
        case let int1 as Int:
            if let int2 = value2 as? Int {
                return int1 == int2
            } else {
                return false
            }
        case let int32_1 as Int32:
            if let int32_2 = value2 as? Int32 {
                return int32_1 == int32_2
            } else {
                return false
            }
        case let int64_1 as Int64:
            if let int64_2 = value2 as? Int64 {
                return int64_1 == int64_2
            } else {
                return false
            }
        case let float32_1 as Float32:
            if let float32_2 = value2 as? Float32 {
                return float32_1 == float32_2
            } else {
                return false
            }
        case let float64_1 as Float64:
            if let float64_2 = value2 as? Float64 {
                return float64_1 == float64_2
            } else {
                return false
            }
        case let bool1 as Bool:
            if let bool2 = value2 as? Bool {
                return bool1 == bool2
            } else {
                return false
            }
        default:
            throw DataClientError.InvalidValue
        }
    }

    private func logSetAttributes(_ attributes: [String: Any]) {
        // TODO: Sorting is good for testing but bad for performance
        for (attribute, value) in attributes.sorted(by: {$0.key < $1.key}) {
            if value is UnsetSentinel {
                logUnsetAttribute(attribute)
            } else {
                logSetAttribute(attribute, value)
            }
        }
    }

    private func logSetAttribute(_ attribute: String, _ value: Any) {
        let record = SetAttribute(attribute: attribute, value: value)
        records.append(record)
    }

    private func logUnsetAttribute(_ attribute: String) {
        let record = UnsetAttribute(attribute: attribute)
        records.append(record)
    }

    private func setEventParameter(_ parameter: String, _ value: Any) throws {
        lock.lock()
        defer { lock.unlock() }

        guard currentEvent != nil else {
            throw DataClientError.InvalidState
        }

        if currentEventEnabled {
            currentParameters.append((parameter, value))
        }
    }

    class Uploader: Thread {
        init(dataClient: SimpleDataClient, url: URL, eventBatchSize: Int, eventBatchDuration: TimeInterval) {
            self.dataClient         = dataClient
            self.url                = url
            self.eventBatchSize     = eventBatchSize
            self.eventBatchDuration = eventBatchDuration
        }

        let eventBatchSize     : Int
        let eventBatchDuration : TimeInterval
        var dataClient         : SimpleDataClient
        var url                : URL

        override func main() {
            let dc = dataClient
            dc.newEvent.lock()
            while true {
                while !dc.shouldFlush && dc.records.count < eventBatchSize {
                    if !dc.newEvent.wait(until: Date(timeIntervalSinceNow: eventBatchDuration)) {
                        break
                    }
                }

                dc.shouldFlush = false
                if dc.records.count > 0 {
                    let records = try! dc._flush()
                    dc.newEvent.unlock()
                    process(records)
                    dc.newEvent.lock()
                }
            }
        }

        func process(_ records: [Record]) {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/x-ndjson", forHTTPHeaderField: "Content-Type")

            let output = Buffer()
            let json   = JsonSerializer(output: output)
            for r in records {
                r.print(output: json)
            }
            let body = output.buffer.data(using: .utf8)!
            let task = URLSession.shared.uploadTask(with: request, from: body)
            task.resume()
        }
    }
}

protocol Record {
    func print(output: DataSerializer)
}

struct UnsetSentinel {}

class SetAttribute: Record {
    init(attribute: String, value: Any) {
        self.attribute = attribute
        self.value     = value
    }

    var attribute: String
    var value: Any

    func print(output: DataSerializer) {
        try! output.startObject();
        try! output.objectKey(attribute);
        try! output.value(value);
        try! output.endObject();
    }
}

class UnsetAttribute: Record {
    init(attribute: String) {
        self.attribute = attribute
    }

    var attribute: String

    func print(output: DataSerializer) {
        try! output.startObject();
        try! output.objectKey(attribute);
        try! output.nullValue()
        try! output.endObject();
    }
}

public class Event: Record {
    init(event: String, parameters: [(String, Any)]) {
        self.event      = event
        self.parameters = parameters
    }

    var event: String
    var parameters: [(String, Any)]

    func print(output: DataSerializer) {
        try! output.startArray();
        try! output.stringValue(event)
        try! output.startObject()
        for (parameter, value) in parameters {
            try! output.objectKey(parameter);
            try! output.value(value)
        }
        try! output.endObject()
        try! output.endArray();
    }
}
