import Foundation

public class AnalyticsClient {
    var dataClient: SimpleDataClient?
    var sessionManager: SessionManager?

    public convenience init(
        configEndpoint:  String,
        configComplete:  @escaping (AnalyticsClient, Bool) -> Void,
        randomGenerator: @escaping () -> Double
    ) {
        self.init(
            configEndpoint: URL(string: configEndpoint)!,
            configComplete: configComplete,
            randomGenerator: randomGenerator
        )
    }

    public convenience init(
        configEndpoint:  URL,
        configComplete:  @escaping (AnalyticsClient, Bool) -> Void,
        randomGenerator: @escaping () -> Double
    ) {
        self.init(
            configCallback: { callback in
                let request = URLRequest(url: configEndpoint)
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if let data = data {
                        let config = try! RemoteConfig(data)
                        callback(config)
                    } else {
                        callback(nil)
                    }
                }
                task.resume()
            },
            configComplete: configComplete,
            randomGenerator: randomGenerator
        )
    }

    public init(
        configCallback: (@escaping (RemoteConfig?) -> Void) -> Void,
        configComplete:  @escaping (AnalyticsClient, Bool) -> Void,
        randomGenerator: @escaping () -> Double
    ) {
        let dataClient = SimpleDataClient(config: DefaultDataClientConfig())
        self.dataClient = dataClient

        dataClient.setEventHook { dataClient in
            try! dataClient.eventParameter("timestamp", Date().timeIntervalSince1970)
        }

        let sessionManager = SessionManager(sessionTimeout: TimeInterval(RemoteConfig.sessionTimeoutDefault)) { sessionId in
            try! dataClient.set("sessionId", sessionId.uuidString)
            try! dataClient.event("start")
        }
        self.sessionManager = sessionManager

        configCallback { config in
            var enabled = false
            if let config = config {
                enabled = (
                    config.pluginEnabled &&
                    (randomGenerator() <= config.sampleRate)
                )
                if enabled {
                    dataClient.config = config // TODO: lock
                    sessionManager.sessionTimeout = TimeInterval(config.sessionTimeout)
                    SimpleDataClient.Uploader(
                        dataClient: dataClient,
                        url: URL(string: config.dataEndpoint)!,
                        eventBatchSize: config.flushBatchSize,
                        eventBatchDuration: TimeInterval(config.flushFrequency)
                    ).start()
                    configComplete(self, true)
                }
            }
            if !enabled {
                self.dataClient = nil // TODO: lock
                configComplete(self, false)
            }
        }
     }


    public func set(_ attribute: String, _ value: Bool) throws {
        try dataClient?.set(attribute, value)
    }

    public func set(_ attribute: String, _ value: Float64) throws {
        try dataClient?.set(attribute, value)
    }

    public func set(_ attribute: String, _ value: Float32) throws {
        try dataClient?.set(attribute, value)
    }

    public func set(_ attribute: String, _ value: Int16) throws {
        try dataClient?.set(attribute, value)
    }

    public func set(_ attribute: String, _ value: Int32) throws {
        try dataClient?.set(attribute, value)
    }

    public func set(_ attribute: String, _ value: Int64) throws {
        try dataClient?.set(attribute, value)
    }

    public func set(_ attribute: String, _ value: Int) throws {
        try dataClient?.set(attribute, value)
    }

    public func set(_ attribute: String, _ value: String) throws {
        try dataClient?.set(attribute, value)
    }

    public func unset(_ attribute: String) throws {
        try dataClient?.unset(attribute)
    }

    public func eventParameter(_ parameter: String, _ value: Any) throws {
        try dataClient?.eventParameter(parameter, value)
    }

    public func eventParameter(_ parameter: String, _ value: Bool) throws {
        try dataClient?.eventParameter(parameter, value)
    }

    public func eventParameter(_ parameter: String, _ value: Float64) throws {
        try dataClient?.eventParameter(parameter, value)
    }

    public func eventParameter(_ parameter: String, _ value: Float32) throws {
        try dataClient?.eventParameter(parameter, value)
    }

    public func eventParameter(_ parameter: String, _ value: Int16) throws {
        try dataClient?.eventParameter(parameter, value)
    }

    public func eventParameter(_ parameter: String, _ value: Int32) throws {
        try dataClient?.eventParameter(parameter, value)
    }

    public func eventParameter(_ parameter: String, _ value: Int64) throws {
        try dataClient?.eventParameter(parameter, value)
    }

    public func eventParameter(_ parameter: String, _ value: Int) throws {
        try dataClient?.eventParameter(parameter, value)
    }

    public func event(_ event: String) throws {
        try dataClient?.event(event)
    }

    public func event(_ event: String, _ key1: String, _ value1: Any) throws {
        try dataClient?.event(event, key1, value1)
    }

    public func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any) throws {
        try dataClient?.event(event, key1, value1, key2, value2)
    }

    public func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any) throws {
        try dataClient?.event(event, key1, value1, key2, value2, key3, value3)
    }

    public func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any) throws {
        try dataClient?.event(event, key1, value1, key2, value2, key3, value3, key4, value4)
    }

    public func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any) throws {
        try dataClient?.event(event, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5)
    }

    public func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any) throws {
        try dataClient?.event(event, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6)
    }

    public func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any, _ key7: String, _ value7: Any) throws {
        try dataClient?.event(event, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6, key7, value7)
    }

    public func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any, _ key7: String, _ value7: Any, _ key8: String, _ value8: Any) throws {
        try dataClient?.event(event, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6, key7, value7, key8, value8)
    }

    public func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any, _ key7: String, _ value7: Any, _ key8: String, _ value8: Any, _ key9: String, _ value9: Any) throws {
        try dataClient?.event(event, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6, key7, value7, key8, value8, key9, value9)
    }

    public func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any, _ key7: String, _ value7: Any, _ key8: String, _ value8: Any, _ key9: String, _ value9: Any, _ key10: String, _ value10: Any) throws {
        try dataClient?.event(event, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6, key7, value7, key8, value8, key9, value9, key10, value10)
    }

    public func event(_ event: String, _ params: [String: Any]) throws {
        try dataClient?.event(event, params)
    }

    public func flush() {
        dataClient?.flush()
    }

    public func pause() {
        sessionManager?.pause()
    }

    public func unpause() {
        sessionManager?.unpause()
    }
}
