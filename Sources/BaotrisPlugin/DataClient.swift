import Foundation

public protocol DataClient {
    func set(_ attribute: String, _ value: Bool   ) throws
    func set(_ attribute: String, _ value: Float64) throws
    func set(_ attribute: String, _ value: Float32) throws
    func set(_ attribute: String, _ value: Int16  ) throws
    func set(_ attribute: String, _ value: Int32  ) throws
    func set(_ attribute: String, _ value: Int64  ) throws
    func set(_ attribute: String, _ value: Int    ) throws
    func set(_ attribute: String, _ value: String ) throws
    func unset(_ attribute: String) throws

    func startEvent(_ event: String) throws
    func eventParameter(_ parameter: String, _ value: Bool   ) throws
    func eventParameter(_ parameter: String, _ value: Float64) throws
    func eventParameter(_ parameter: String, _ value: Float32) throws
    func eventParameter(_ parameter: String, _ value: Int16  ) throws
    func eventParameter(_ parameter: String, _ value: Int32  ) throws
    func eventParameter(_ parameter: String, _ value: Int64  ) throws
    func eventParameter(_ parameter: String, _ value: Int    ) throws
    func eventParameter(_ parameter: String, _ value: String ) throws
    func endEvent() throws

    func setEventHook(_ eventHook: @escaping (DataClient) -> Void)
}

public enum DataClientError: Error {
    case InvalidState, InvalidValue
}

public extension DataClient {
    func eventParameter(_ parameter: String, _ value: Any) throws {
        switch value {
        case let string as String:
            try eventParameter(parameter, string)
        case let int as Int:
            try eventParameter(parameter, Int64(int))
        case let int16 as Int16:
            try eventParameter(parameter, Int32(int16))
        case let int32 as Int32:
            try eventParameter(parameter, int32)
        case let int64 as Int64:
            try eventParameter(parameter, int64)
        case let float32 as Float32:
            try eventParameter(parameter, float32)
        case let float64 as Float64:
            try eventParameter(parameter, float64)
        case let bool as Bool:
            try eventParameter(parameter, bool)
        default:
            throw DataClientError.InvalidValue
        }
    }

    func event(_ event: String) throws {
        try startEvent(event)
        try endEvent()
    }

    func event(_ event: String, _ key1: String, _ value1: Any) throws {
        try startEvent(event)
        try eventParameter(key1, value1)
        try endEvent()
    }

    func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any) throws {
        try startEvent(event)
        try eventParameter(key1, value1)
        try eventParameter(key2, value2)
        try endEvent()
    }

    func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any) throws {
        try startEvent(event)
        try eventParameter(key1, value1)
        try eventParameter(key2, value2)
        try eventParameter(key3, value3)
        try endEvent()
    }

    func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any) throws {
        try startEvent(event)
        try eventParameter(key1, value1)
        try eventParameter(key2, value2)
        try eventParameter(key3, value3)
        try eventParameter(key4, value4)
        try endEvent()
    }

    func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any) throws {
        try startEvent(event)
        try eventParameter(key1, value1)
        try eventParameter(key2, value2)
        try eventParameter(key3, value3)
        try eventParameter(key4, value4)
        try eventParameter(key5, value5)
        try endEvent()
    }

    func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any) throws {
        try startEvent(event)
        try eventParameter(key1, value1)
        try eventParameter(key2, value2)
        try eventParameter(key3, value3)
        try eventParameter(key4, value4)
        try eventParameter(key5, value5)
        try eventParameter(key6, value6)
        try endEvent()
    }

    func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any, _ key7: String, _ value7: Any) throws {
        try startEvent(event)
        try eventParameter(key1, value1)
        try eventParameter(key2, value2)
        try eventParameter(key3, value3)
        try eventParameter(key4, value4)
        try eventParameter(key5, value5)
        try eventParameter(key6, value6)
        try eventParameter(key7, value7)
        try endEvent()
    }

    func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any, _ key7: String, _ value7: Any, _ key8: String, _ value8: Any) throws {
        try startEvent(event)
        try eventParameter(key1, value1)
        try eventParameter(key2, value2)
        try eventParameter(key3, value3)
        try eventParameter(key4, value4)
        try eventParameter(key5, value5)
        try eventParameter(key6, value6)
        try eventParameter(key7, value7)
        try eventParameter(key8, value8)
        try endEvent()
    }

    func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any, _ key7: String, _ value7: Any, _ key8: String, _ value8: Any, _ key9: String, _ value9: Any) throws {
        try startEvent(event)
        try eventParameter(key1, value1)
        try eventParameter(key2, value2)
        try eventParameter(key3, value3)
        try eventParameter(key4, value4)
        try eventParameter(key5, value5)
        try eventParameter(key6, value6)
        try eventParameter(key7, value7)
        try eventParameter(key8, value8)
        try eventParameter(key9, value9)
        try endEvent()
    }

    func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any, _ key7: String, _ value7: Any, _ key8: String, _ value8: Any, _ key9: String, _ value9: Any, _ key10: String, _ value10: Any) throws {
        try startEvent(event)
        try eventParameter(key1, value1)
        try eventParameter(key2, value2)
        try eventParameter(key3, value3)
        try eventParameter(key4, value4)
        try eventParameter(key5, value5)
        try eventParameter(key6, value6)
        try eventParameter(key7, value7)
        try eventParameter(key8, value8)
        try eventParameter(key9, value9)
        try eventParameter(key10, value10)
        try endEvent()
    }

    func event(_ event: String, _ params: [String: Any]) throws {
        try startEvent(event)
        for (parameter, value) in params {
            try eventParameter(parameter, value)
        }
        try endEvent()
    }
}
