import Foundation

public struct RemoteConfig: DataClientConfig {
    static let pluginEnabledDefault  = true
    static let sessionTimeoutDefault = 30*60
    static let sampleRateDefault     = 1.0
    static let flushFrequencyDefault = 2*60
    static let flushBatchSizeDefault = 25

    init(_ string: String) throws {
        let data  = string.data(using: .utf8, allowLossyConversion: true)!
        self.json = try JSONDecoder().decode(RemoteConfigJson.self, from: data)
    }

    init(_ data: Data) throws {
        self.json = try JSONDecoder().decode(RemoteConfigJson.self, from: data)
    }

    // TODO: Rename flush stuff
    
    var json           : RemoteConfigJson
    var dataEndpoint   : String { json.dataEndpoint }
    var pluginEnabled  : Bool   { json.pluginEnabled  ?? RemoteConfig.pluginEnabledDefault  }
    var sessionTimeout : Int    { json.sessionTimeout ?? RemoteConfig.sessionTimeoutDefault }
    var sampleRate     : Double { json.sampleRate     ?? RemoteConfig.sampleRateDefault     }
    var flushFrequency : Int    { json.flushCondition?.frequency    ?? RemoteConfig.flushFrequencyDefault }
    var flushBatchSize : Int    { json.flushCondition?.numOfRecords ?? RemoteConfig.flushBatchSizeDefault }

    public func attributeEnabled(_ attribute: String) -> Bool {
        if let attributes = json.attributes {
            for attributeJson in attributes {
                if attributeJson.attribute == attribute {
                    return attributeJson.enabled
                }
            }
        }
        return true
    }

    public func eventEnabled(_ event: String) -> Bool {
        if let events = json.events {
            for eventJson in events {
                if eventJson.event == event {
                    return eventJson.enabled
                }
            }
        }
        return true
    }
    
    public func schemaMatch(_ event: Event) -> Bool {
        if let events = json.events {
            for eventJson in events {
                if eventJson.event == event.event {
                    if let parameters = eventJson.parameters {
                        for parameterJson in parameters {
                            try! parameterMatch(parameterJson, event)
                        }
                    }
                }
            }
        }
        return true
    }
    
    private func parameterMatch(_ parameterJson: ParameterJson, _  event: Event) throws {
        for (eventParameter, eventValue) in event.parameters {
            if parameterJson.parameter == eventParameter {
                let parameterType = try! getTypeOf(eventValue)
                if parameterType == parameterJson.type {
                    return
                } else {
                    throw SchemaError.typeMismatchError("Type mismatch. Expected \(parameterJson.type), actual \(parameterType).")
                }
            }
        }
        if parameterJson.required {
            throw SchemaError.missingRequiredFieldError("Missing required parameter: \(parameterJson.parameter)")
        }
    }
    
    private func getTypeOf(_ parameterValue: Any) throws -> String {
        switch parameterValue {
        case _ as String:
            return "String"
        case _ as Int:
            return "Int"
        case _ as Int32:
            return "Int"
        case _ as Int64:
            return "Int"
        case _ as Float32:
            return "Float"
        case _ as Float64:
            return "Float"
        case _ as Bool:
            return "Bool"
        default:
            throw DataClientError.InvalidValue
        }
    }
 }

struct RemoteConfigJson: Decodable {
    var pluginEnabled  : Bool?
    var dataEndpoint   : String
    var sessionTimeout : Int?
    var sampleRate     : Double?
    var flushCondition : FlushConditionJson?
    var attributes     : [AttributeJson]?
    var events         : [EventJson]?
}

struct FlushConditionJson: Decodable {
    var frequency      : Int?
    var numOfRecords   : Int?
}

struct AttributeJson: Decodable {
    var attribute      : String
    var enabled        : Bool
}

struct EventJson: Decodable {
    var event          : String
    var enabled        : Bool
    var parameters     : [ParameterJson]?
}

struct ParameterJson: Decodable {
    var parameter      : String
    var required       : Bool
    var type           : String //TODO: make enum?
}

enum SchemaError: Error {
    case typeMismatchError(String)
    case missingRequiredFieldError(String)
}
