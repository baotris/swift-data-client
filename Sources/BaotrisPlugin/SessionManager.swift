import Foundation

class SessionManager {
    var sessionId: UUID?
    var isPaused: Bool = false
    var pauseTimestamp: Date = Date()
    var listeners = [(uuid: UUID) -> Void]()
    public var sessionTimeout: TimeInterval

    init(sessionTimeout: TimeInterval, listener: @escaping (_ sessionId: UUID) -> Void) {
        self.sessionTimeout = sessionTimeout
        addListener(listener)
        generateSessionId()
    }

    func pause() {
        setPauseTimestamp(Date())
    }

    func unpause() {
        setUnpauseTimestamp(Date())
    }
    
    func setPauseTimestamp(_ pauseTimestamp: Date) {
        if !isPaused {
            self.pauseTimestamp = pauseTimestamp
            isPaused = true
        }
    }

    func setUnpauseTimestamp(_ unpauseTimestamp: Date) {
        if isPaused {
            let pauseTime = unpauseTimestamp.timeIntervalSince(pauseTimestamp)
            if pauseTime >= sessionTimeout {
                generateSessionId()
            }
            isPaused = false
        }
    }

    func addListener(_ listener: @escaping (_ sessionId: UUID) -> Void) {
        listeners.append(listener)
    }
    
    func generateSessionId() {
        let sessionId = UUID()
        self.sessionId = sessionId
        for listener in listeners {
            listener(sessionId)
        }
    }
}
