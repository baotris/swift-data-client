import Foundation
#if canImport(UIKit)
import UIKit
#endif

public class BaotrisPlugin {
    private static var client: AnalyticsClient?

    public static func initialize(configEndpoint: String) {
        initialize(configEndpoint: URL(string: configEndpoint)!)
    }

    public static func initialize(configEndpoint: URL) {
        self.client = AnalyticsClient(
            configEndpoint: configEndpoint,
            configComplete: { (client, enabled) in
                if enabled {
                    #if canImport(UIKit)
                    NotificationCenter.default.addObserver(
                        forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: nil
                    ) { _ in
                        client.flush()
                        client.pause()
                    }

                    NotificationCenter.default.addObserver(
                        forName: UIApplication.didBecomeActiveNotification, object: nil, queue: nil
                    ) { _ in
                        client.unpause()
                    }
                    #endif
                }
            },
            randomGenerator: { Double.random(in: 0 ..< 1) }
        )
    }

    public static func set(_ attribute: String, _ value: Bool) throws {
        try client?.set(attribute, value)
    }

    public static func set(_ attribute: String, _ value: Float64) throws {
        try client?.set(attribute, value)
    }

    public static func set(_ attribute: String, _ value: Float32) throws {
        try client?.set(attribute, value)
    }

    public static func set(_ attribute: String, _ value: Int16) throws {
        try client?.set(attribute, value)
    }

    public static func set(_ attribute: String, _ value: Int32) throws {
        try client?.set(attribute, value)
    }

    public static func set(_ attribute: String, _ value: Int64) throws {
        try client?.set(attribute, value)
    }

    public static func set(_ attribute: String, _ value: Int) throws {
        try client?.set(attribute, value)
    }

    public static func set(_ attribute: String, _ value: String) throws {
        try client?.set(attribute, value)
    }

    public static func unset(_ attribute: String) throws {
        try client?.unset(attribute)
    }

    public static func eventParameter(_ parameter: String, _ value: Any) throws {
        try client?.eventParameter(parameter, value)
    }

    public static func eventParameter(_ parameter: String, _ value: Bool) throws {
        try client?.eventParameter(parameter, value)
    }

    public static func eventParameter(_ parameter: String, _ value: Float64) throws {
        try client?.eventParameter(parameter, value)
    }

    public static func eventParameter(_ parameter: String, _ value: Float32) throws {
        try client?.eventParameter(parameter, value)
    }

    public static func eventParameter(_ parameter: String, _ value: Int16) throws {
        try client?.eventParameter(parameter, value)
    }

    public static func eventParameter(_ parameter: String, _ value: Int32) throws {
        try client?.eventParameter(parameter, value)
    }

    public static func eventParameter(_ parameter: String, _ value: Int64) throws {
        try client?.eventParameter(parameter, value)
    }

    public static func eventParameter(_ parameter: String, _ value: Int) throws {
        try client?.eventParameter(parameter, value)
    }

    public static func event(_ event: String) throws {
        try client?.event(event)
    }

    public static func event(_ event: String, _ key1: String, _ value1: Any) throws {
        try client?.event(event, key1, value1)
    }

    public static func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any) throws {
        try client?.event(event, key1, value1, key2, value2)
    }

    public static func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any) throws {
        try client?.event(event, key1, value1, key2, value2, key3, value3)
    }

    public static func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any) throws {
        try client?.event(event, key1, value1, key2, value2, key3, value3, key4, value4)
    }

    public static func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any) throws {
        try client?.event(event, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5)
    }

    public static func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any) throws {
        try client?.event(event, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6)
    }

    public static func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any, _ key7: String, _ value7: Any) throws {
        try client?.event(event, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6, key7, value7)
    }

    public static func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any, _ key7: String, _ value7: Any, _ key8: String, _ value8: Any) throws {
        try client?.event(event, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6, key7, value7, key8, value8)
    }

    public static func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any, _ key7: String, _ value7: Any, _ key8: String, _ value8: Any, _ key9: String, _ value9: Any) throws {
        try client?.event(event, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6, key7, value7, key8, value8, key9, value9)
    }

    public static func event(_ event: String, _ key1: String, _ value1: Any, _ key2: String, _ value2: Any, _ key3: String, _ value3: Any, _ key4: String, _ value4: Any, _ key5: String, _ value5: Any, _ key6: String, _ value6: Any, _ key7: String, _ value7: Any, _ key8: String, _ value8: Any, _ key9: String, _ value9: Any, _ key10: String, _ value10: Any) throws {
        try client?.event(event, key1, value1, key2, value2, key3, value3, key4, value4, key5, value5, key6, value6, key7, value7, key8, value8, key9, value9, key10, value10)
    }

    public static func event(_ event: String, _ params: [String: Any]) throws {
        try client?.event(event, params)
    }

    public static func flush() {
        client?.flush()
    }
}
