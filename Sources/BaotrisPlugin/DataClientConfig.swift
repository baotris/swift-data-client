import Foundation

public protocol DataClientConfig {
    func attributeEnabled(_ attribute: String) -> Bool
    func eventEnabled(_ event: String) -> Bool
    func schemaMatch(_ event: Event) -> Bool
}

public class DefaultDataClientConfig: DataClientConfig {
    public func attributeEnabled(_ attribute: String) -> Bool { return true }
    public func eventEnabled(_ event: String) -> Bool { return true }
    public func schemaMatch(_ event: Event) -> Bool {return true}
}
