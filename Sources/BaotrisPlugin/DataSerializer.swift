import Foundation

protocol DataSerializer {
    func stringValue (_ value: String)  throws
    func intValue    (_ value: Int32)   throws
    func longValue   (_ value: Int64)   throws
    func floatValue  (_ value: Float32) throws
    func doubleValue (_ value: Float64) throws
    func boolValue   (_ value: Bool)    throws
    func nullValue() throws

    func startArray() throws
    func endArray()   throws

    func startObject() throws
    func objectKey(_ key: String) throws
    func endObject() throws
}

enum DataSerializerError: Error {
    case InvalidState, InvalidValue
}

extension DataSerializer {
    func value(_ val: Any?) throws {
        if let val = val {
            try value(val)
        } else {
            try nullValue()
        }
    }

    func value(_ value: Any) throws {
        switch value {
        case let string as String:
            try stringValue(string)
        case let int16 as Int16:
            try intValue(Int32(int16))
        case let int32 as Int32:
            try intValue(int32)
        case let int64 as Int64:
            try longValue(int64)
        case let int as Int:
            try longValue(Int64(int))
        case let float32 as Float32:
            try floatValue(float32)
        case let float64 as Float64:
            try doubleValue(float64)
        case let bool as Bool:
            try boolValue(bool)
        default:
            throw DataSerializerError.InvalidValue
        }
    }
}
