import Foundation

// TODO: Is this not a built-in class already?
class Buffer: TextOutputStream {
    var buffer = ""
    func write(_ value: String) {
        buffer.write(value)
    }
}
