import Foundation

class JsonSerializer: DataSerializer {
    init(output: TextOutputStream) {
        self.output = output
    }

    enum State {
        case Stream, Array, ArrayStart, Object, ObjectStart, ObjectValue
    }

    var state: State = .Stream
    var states: Stack<State> = Stack<State>()
    var output: TextOutputStream

    func stringValue(_ value: String) throws {
        try startValue()
        writeString(value)
        endValue()
    }

    func intValue(_ value: Int32) throws {
        try startValue()
        write(String(value))
        endValue()
    }

    func longValue(_ value: Int64) throws {
        try startValue()
        write(String(value))
        endValue()
    }

    func floatValue(_ value: Float32) throws {
        try startValue()
        write(String(value))
        endValue()
    }

    func doubleValue(_ value: Float64) throws {
        try startValue()
        write(String(value))
        endValue()
    }

    func boolValue(_ value: Bool) throws {
        try startValue()
        write(value ? "true" : "false")
        endValue()
    }

    func nullValue() throws {
        try startValue()
        write("null")
        endValue()
    }

    func startArray() throws {
        try startValue()
        writeChar("[")
        pushState(.ArrayStart)
    }

    func endArray() throws {
        guard state == .ArrayStart || state == .Array else {
            throw DataSerializerError.InvalidState
        }
        writeChar("]")
        try popState()
        endValue()
    }

    func startObject() throws {
        try startValue()
        writeChar("{")
        pushState(.ObjectStart)
    }

    func objectKey(_ key: String) throws {
        guard state == .ObjectStart || state == .Object else {
            throw DataSerializerError.InvalidState
        }
        if (state == .Object) {
            writeChar(",")
        }
        writeString(key)
        writeChar(":")
        state = .ObjectValue
    }

    func endObject() throws {
        guard state == .ObjectStart || state == .Object else {
            throw DataSerializerError.InvalidState
        }
        writeChar("}")
        try popState()
        endValue()
    }

    private func pushState(_ state: State) {
        states.push(self.state)
        self.state = state
    }

    private func popState() throws {
        guard let state = states.pop() else {
            throw DataSerializerError.InvalidState
        }
        self.state = state
    }

    private func startValue() throws {
        guard state != .Object && state != .ObjectStart else {
            throw DataSerializerError.InvalidState
        }
        if (state == .Array) {
            writeChar(",")
        } else if (state == .ArrayStart) {
            state = .Array
        }
    }

    private func endValue() {
        if (state == .Stream) {
            writeChar("\n")
        } else if (state == .ObjectValue){
            state = .Object
        }
    }

    private func writeString(_ value: String) {
        writeChar("\"")
        for ch in value {
            switch ch {
            case "\"",
                 "\\":
                writeChar("\\")
                writeChar(ch)
            case "\u{8}":
                writeChar("\\")
                writeChar("b")
            case "\u{9}":
                writeChar("\\")
                writeChar("t")
            case "\u{10}":
                writeChar("\\")
                writeChar("n")
            case "\u{12}":
                writeChar("\\")
                writeChar("f")
            case "\u{13}":
                writeChar("\\")
                writeChar("r")
            default:
                if ch >= " " && ch <= "~" {
                    writeChar(ch)
                } else {
                    for v in ch.unicodeScalars {
                        writeChar("\\")
                        writeChar("u")
                        write(String(format: "%04X", v.value))
                    }
                }
            }
        }
        writeChar("\"")
    }

    private func write(_ value: String) {
        output.write(value)
    }

    private func writeChar(_ value: Character) {
        output.write(String(value))
    }
}
